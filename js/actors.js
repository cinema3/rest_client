async function getActeurs() {
  const data = await fetch("http://127.0.0.1:3000/actor");
  const retour = await data.json();
  const datajson = retour.data;
  const template = document.getElementById("templateActeur");
  const container = document.getElementById("container");

      datajson.forEach(acteur => {
        const content = template.content.cloneNode(true);
        content.querySelector(".nom").innerText = acteur.name;
        if (acteur.age != null) {
          content.querySelector(".dob").innerText = `${acteur.age} ans`;
        }
        else {
          content.querySelector(".dob").innerText = ``;
        }
        content.querySelector(".count").dataset.count = acteur.movie_count;
        content.querySelector(".info a").href += `?actor=${acteur.id}`;

        const n = content.querySelector(".nationalities");
        acteur.nationalities.forEach(function (nationality) {
          const flag = document.createElement("img");
          flag.classList.add("flag");
          flag.setAttribute("src", `https://res.cloudinary.com/lp-amio/image/upload/c_scale,w_16/flag/${nationality}.png`);
          flag.setAttribute("title", nationality);
          n.appendChild(flag);
        })
        content.querySelector(".portrait>img").src =
          `https://res.cloudinary.com/lp-amio/image/upload/c_scale,d_actor_default.jpg,w_80/actor/${acteur.id}.jpg`;
        content.querySelector(".portrait>img")
          .setAttribute("alt", acteur.name);


        /*let rect = content.getElementById("film5654");
        console.log(rect);
        rect.querySelector("rect").setAttribute('width', Math.random() * 80);
        */
        container.appendChild(content);
      });
}

getActeurs();

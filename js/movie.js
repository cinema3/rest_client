time_convert = (num) => `${Math.floor(num / 60)}h ${num % 60}min`;

async function getFilms() {
  const urlParams = new URLSearchParams(window.location.search);
  const actor = urlParams.get('actor')
  const data = await fetch(`http://localhost:3000/actor/${actor}/movie`);
  const retour = await data.json()
  let datajson = retour.data;

  let template = document.getElementById("templateFilm");
  let container = document.getElementById("container2");
  datajson.forEach(film => {
    let content = template.content.cloneNode(true);
    content.querySelector(".titre").innerText = film.title;
    content.querySelector(".release").innerText = new Date(film.release).toLocaleDateString('fr-FR',
      { year: 'numeric', month: 'long', day: 'numeric' });
    content.querySelector(".runtime").innerText = time_convert(film.runtime);
    content.querySelector(".director").innerText = film.director;
    content.querySelector(".poster>img").src =
      `https://res.cloudinary.com/lp-amio/image/upload/c_fill,g_south,h_225,w_150/movie/${film.id}.jpg`;
    container.appendChild(content);
  });
}

getFilms();
